#from mlxtend.frequent_patterns import association_rules
#import pyfpgrowth as fpg
import pandas as pd
import numpy as np
import pickle as pkl
from itertools import combinations
pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_columns', None)
import random
from random import randint

associationRules = pd.read_pickle('assoctiation-rules-processed.pkl')

def random_combination(iterable, r):
    "Random selection from itertools.combinations(iterable, r)"
    pool = tuple(iterable)
    n = len(pool)
    indices = sorted(random.sample(xrange(n), r))
    return tuple(pool[i] for i in indices)


def removeRepeated(inp, rules):
    idx=0
    rules.reset_index()
    dropIndex = []
    a=0
    b=0
    inp = set(inp)
    print (inp)
    for i,row in rules.iterrows():
        consequents = set(row['consequents'])
#         print consequents,
        consequents = consequents.difference(inp)
#         print consequents
        if len(consequents) != 0:
            for c in rules.loc[i, 'consequents']:
                if c not in consequents:
                    rules.loc[i,'consequents'].remove(c)
                    a+=1
        else:
            dropIndex.append(idx)
            b+=1
        idx += 1
    
    rules = rules.drop(rules.index[dropIndex])
    return rules


def parseInput(inp):
    op = []
    if 'age' in inp:
        age = inp['age']
        inp.pop('age',None)
        if age < 25:
            op.append('young')
        elif age < 35:
            op.append('middle')
        else:
            op.append('elder')
    
    if 'sex' in inp:
        op.append(inp['sex'])
        inp.pop('sex',None)

    if 'income' in inp:
        income = inp['income']
        inp.pop('income',None)
        if income < 20:
            op.append('10k-20k')
        else:
            op.append('20k-30k')

    if 'pmethod' in inp:
        op.append(inp['pmethod'])
        inp.pop('pmethod',None)

    if 'homeown' in inp:
        if inp['homeown'] == True:
            op.append("home-owner")
        else:
            op.append('non-owner')
        inp.pop('homeown',None)

    for key in inp:
        op.append(key)

    return op

def recommend(inp, nRules = 5, minSupport = 0.003, minConfidence = 0.2, second=False):  
    inp = parseInput(inp)
    
    rules = pd.DataFrame(columns=associationRules.columns)
    rules = rules.append(associationRules[associationRules.antecedants == set(inp)])
    for l in range(len(inp)):
        for subset in combinations(inp, l):
            rules = rules.append(associationRules[associationRules.antecedants == set(subset)])
    
#     print '1:', len(rules)
    rules = removeRepeated(inp, rules)
#     print '2:', len(rules)
    rules = rules[(rules.support >= minSupport) & (rules.confidence >= minConfidence)]
#     print '3:', len(rules)
    if (len(rules) < 4 and (second == False)):
        return recommend(inp, nRules, minSupport=0, minConfidence=0, second=True)

    rules.drop(['leverage'], axis=1, inplace=True)
    rules = rules.sort_values(by=['lift'], ascending=False)[:min(nRules,len(rules))]   
    print (rules)

    res = []
    for i,_ in rules.iterrows():
        res.append(list(rules.loc[i,'consequents']))

    flat_list = set([i for s in res for i in s])
    return flat_list

avgAcc = None

def getAccuracy(inp, nRules = 10): 
    cnt = 0
    accRules = recommend(inp, nRules = nRules)
    accRules['Accuracy'] = 0
    accRules['Count'] = 0

    for j, _ in accRules.iterrows():
        acc = 0
        colcnt = len(accRules.loc[j,'consequents'])

        l=0
        r=0
        for i, _ in df.iterrows():
            active = True
            inpSet = set(inp)
            for column in inpSet:
                if df.loc[i,column] == 'F':
                    active = False

            if active:
                cnt += 1
                for col in accRules.loc[j,'consequents']:
                    if df.loc[i,col] == 'T':
                        l += 1.0
                    else:
                        r += 1.0
        try:
            accRules.loc[j,'Accuracy'] = l/(l+r)
        except:
            accRules.loc[j,'Accuracy'] = avgAcc
            
        accRules['Count'] = cnt/len(accRules)
    
    return accRules
