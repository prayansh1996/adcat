from django.db import models

# Create your models here.
"""
GENDER_CHOICES = (
		('M', 'Male'),
		('F', 'Female'),
		('O', 'Other'),
)

PAYMENT_METHODS = (
	('C', 'Cash'),
	('Q', 'Cheque'),
)


class Profile(models.Model):
	payment = models.CharField('Payment Methods', choices=PAYMENT_METHODS, max_length=1, default=PAYMENT_METHODS[0][0])



class Customer(models.Model):
	first_name = models.CharField("First Name", max_length=128, blank=True)
	last_name = models.CharField("Last Name", max_length=128, blank=True)
	age = models.SmallPositiveInteger(blank=False)
	gender = models.CharField('Gender', choices=GENDER_CHOICES, max_length=1, default=GENDER_CHOICES[0][0])
	income = models.SmallPositiveInteger(blank=False)
	profile = models.ForeignKey(Profile, related_name="customers")
"""
