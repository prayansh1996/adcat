from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from rater import recommend

# Create your views here.

def landing(request):
	return render(request, 'index.html', {})

@csrf_exempt
@require_http_methods(['GET', 'POST'])
def questionnaire(request):
	if request.method == 'POST':
		err = {
			'message': "Error Occurred. Please enter input correctly.",
			'status': 400,
		}

#		try:
		inputs = {}
		# Extract form inputs
		POST = request.POST.copy()
		POST.pop('csrfmiddlewaretoken')
		for key in POST:
			if key == 'dob':
				if not POST.get(key):
					continue
				else:
					from datetime import datetime, date
					from urllib.parse import unquote_plus
					d = unquote_plus(POST.get(key))
					dob = datetime.strptime(d, '%b %d, %Y')
					today = date.today()
					inputs['age'] = int(today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day)))
					continue
			elif key == 'pmethod':
				inputs[key] = ('cheque' if POST.get(key) == 'Q' else 'cash')
			elif key == 'sex':
				inputs[key] = ('male' if POST.get(key) == 'M' else 'female')
			elif key == 'income':
				inputs[key] = int(POST.get(key))
			elif key.startswith('Recurr'):
				from urllib.parse import unquote_plus
				inputs[unquote_plus(key)] = POST.get(key)
			else:
				if POST.get(key) == 'on':
					POST[key] = True
				inputs[key] = POST.get(key)

#		print(inputs)
		results = recommend(inputs)
		results = [r.title().replace('_', ' ') for r in results]
		
		html = render(request, 'questionnaire.html', {'results': results}).content.decode('utf-8');
		return JsonResponse(status=200, data={'html': html})
#		except Exception as e:
#			print(e)
#			return JsonResponse(status=err['status'], data={'message': err['message']})
	else:
		return render(request, 'questionnaire.html', {})
#		html = render(request, 'questionnaire.html', {}).content.decode('utf-8')
#		return JsonResponse(status=200, data={'html': html})

